package br.com.teste.beblue.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import br.com.teste.beblue.daos.UserDAO;
import br.com.teste.beblue.models.User;

@Controller
public class UserController {
	
	@Autowired
	private UserDAO dao;
	
	@RequestMapping(value="/user", method=RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public String salvar(@RequestBody User user){
		dao.salvaOuAtualiza(user);
		return "index";
	}
	
	@RequestMapping(value="/users", method=RequestMethod.GET)
	@ResponseBody
	public String getAll(){
		List<User> all = dao.getAll();
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		String response = gson.toJson(all);
		return response;
	}
	
}

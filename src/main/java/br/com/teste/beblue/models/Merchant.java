package br.com.teste.beblue.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Merchant {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	private String name; 
	private Double segunda;
	private Double terca;
	private Double quarta;
	private Double quinta;
	private Double sexta;
	private Double sabado;
	private Double domingo;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public Double getSegunda() {
		return segunda;
	}
	public void setSegunda(Double segunda) {
		this.segunda = segunda;
	}
	public Double getTerca() {
		return terca;
	}
	public void setTerca(Double terca) {
		this.terca = terca;
	}
	public Double getQuarta() {
		return quarta;
	}
	public void setQuarta(Double quarta) {
		this.quarta = quarta;
	}
	public Double getQuinta() {
		return quinta;
	}
	public void setQuinta(Double quinta) {
		this.quinta = quinta;
	}
	public Double getSexta() {
		return sexta;
	}
	public void setSexta(Double sexta) {
		this.sexta = sexta;
	}
	public Double getSabado() {
		return sabado;
	}
	public void setSabado(Double sabado) {
		this.sabado = sabado;
	}
	public Double getDomingo() {
		return domingo;
	}
	public void setDomingo(Double domingo) {
		this.domingo = domingo;
	}
	
	
	
}

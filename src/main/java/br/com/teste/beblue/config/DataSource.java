package br.com.teste.beblue.config;

import org.springframework.jdbc.datasource.DriverManagerDataSource;

public class DataSource {
	
	private static String username = "root";
	private static String password = "admin";
	private static String url = "jdbc:mysql://localhost:3306/beblue";
	private static String driver = "com.mysql.jdbc.Driver";
	
	public static DriverManagerDataSource getDataSource(){
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setUsername(username);
		dataSource.setPassword(password);
		dataSource.setUrl(url);
		dataSource.setDriverClassName(driver);
		
		return dataSource;
	}
	
}

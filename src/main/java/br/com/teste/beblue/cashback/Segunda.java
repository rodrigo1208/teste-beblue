package br.com.teste.beblue.cashback;

import java.time.DayOfWeek;

import br.com.teste.beblue.models.Merchant;

public class Segunda extends AbstractCashback {
	
	public Segunda(Merchant merchant) {
		this.porcentagem = merchant.getSegunda();
		this.dayTransaction = DayOfWeek.SATURDAY;
		this.nextDay = new Terca(merchant);
	}
	
}

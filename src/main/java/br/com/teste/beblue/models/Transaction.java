package br.com.teste.beblue.models;

import java.time.LocalDateTime;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
public class Transaction {
	
	@Id
	@GeneratedValue
	private int id;
	
	@JsonProperty("transaction_value")
	private Double transactionValue;
	@JsonProperty("transaction_type")
	private String transactionType;
	@Column
	@JsonProperty("transaction_date")
	private LocalDateTime transactionDate;
	@JsonIgnore
	@JsonProperty("transaction_code")
	private String transactionCode;
	@JsonProperty("user_cpf")
	private Long userCpf;
	@JsonProperty("merchant_id")
	private int merchantId;

	
	public Transaction() {
		this.transactionCode = UUID.randomUUID().toString().replace("-", "");
	}

	public Transaction(Double transactionValue, String transactionType, LocalDateTime transactionDate) {
		super();
		this.transactionValue = transactionValue;
		this.transactionType = transactionType;
		this.transactionDate = transactionDate;
	} 

	public Double getTransactionValue() {
		return transactionValue;
	}

	public void setTransactionValue(Double transactionValue) {
		this.transactionValue = transactionValue;
	}

	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public LocalDateTime getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(LocalDateTime transactionDate) {
		this.transactionDate = transactionDate;
	}

	public String getTransactionCode() {
		return transactionCode;
	}

	public void setTransactionCode(String transactionCode) {
		this.transactionCode = transactionCode;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Long getUserCpf() {
		return userCpf;
	}

	public void setUserCpf(Long userCpf) {
		this.userCpf = userCpf;
	}

	public int getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(int merchantId) {
		this.merchantId = merchantId;
	}

	
	
}

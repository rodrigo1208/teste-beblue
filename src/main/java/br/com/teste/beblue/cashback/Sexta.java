package br.com.teste.beblue.cashback;

import java.time.DayOfWeek;

import br.com.teste.beblue.models.Merchant;

public class Sexta extends AbstractCashback {

	public Sexta(Merchant merchant) {
		this.porcentagem = merchant.getSexta();
		this.dayTransaction = DayOfWeek.FRIDAY;
		this.nextDay = new Sabado(merchant);
	}

}

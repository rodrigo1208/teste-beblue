## Teste Beblue - Desenvolvimento de servidor SpringMVC+Hibernate ##

### Configurações do banco de dados ###

* Banco de dados - MySQL
* Usuário - "root"
* Senha - "admin"

### Testes de cadastro de transações ###

* Teste 1 - OK - Transação salva e cashback calculado com sucesso!
* Teste 2 - OK - Transação salva e cashback calculado com sucesso!
* Teste 3 - OK - Transação salva e cashback calculado com sucesso!
* Teste 4 - OK - Transação salva, cashback não gerado pois o tipo de transação é TP-1

### Consulta de usuários e transações ###

* Consulta de usuários OK - JSON gerado: 
```
#!json

[
  {
    "id": 1,
    "name": "Jo�o Primeiro",
    "userCpf": 11111111111,
    "balance": 12.25
  },
  {
    "id": 2,
    "name": "Maria Segunda",
    "userCpf": 22222222222,
    "balance": 53.52
  },
  {
    "id": 3,
    "name": "Emerson Terceiro",
    "userCpf": 33333333333,
    "balance": 2.25
  },
  {
    "id": 4,
    "name": "Mario Quarto",
    "userCpf": 44444444444,
    "balance": 89.66
  }
]
```
*Consultas de transações OK - JSON gerado:
```
#!json
[
  {
    "user_balance": [
      12.25
    ],
    "transaction_code": [
      "5b7f0c038a3844d58c05aabcbdc4a952"
    ],
    "transaction_data": [
      "2017-01-12T15:05:21"
    ],
    "user_name": [
      "Jo�o Primeiro"
    ],
    "user_cpf": [
      11111111111
    ],
    "transaction_type": [
      "TP_2"
    ]
  },
  {
    "user_balance": [
      12.25
    ],
    "transaction_code": [
      "24f9851ee7d24714ada34766a4516b26"
    ],
    "transaction_data": [
      "2017-01-12T15:16:27"
    ],
    "user_name": [
      "Jo�o Primeiro"
    ],
    "user_cpf": [
      11111111111
    ],
    "transaction_type": [
      "TP_2"
    ]
  },
  {
    "user_balance": [
      12.25
    ],
    "transaction_code": [
      "21a1af10376e4b41891adcb5c85a4eba"
    ],
    "transaction_data": [
      "2017-01-12T15:17:58"
    ],
    "user_name": [
      "Jo�o Primeiro"
    ],
    "user_cpf": [
      11111111111
    ],
    "transaction_type": [
      "TP_2"
    ]
  },
  {
    "user_balance": [
      53.52
    ],
    "transaction_code": [
      "200197e3012f46629ac6649e0c686f86"
    ],
    "transaction_data": [
      "2017-01-12T15:18:32"
    ],
    "user_name": [
      "Maria Segunda"
    ],
    "user_cpf": [
      22222222222
    ],
    "transaction_type": [
      "TP_2"
    ]
  },
  {
    "user_balance": [
      53.52
    ],
    "transaction_code": [
      "5881986434d6430a8ae53775cfddc7b9"
    ],
    "transaction_data": [
      "2017-01-12T15:18:42"
    ],
    "user_name": [
      "Maria Segunda"
    ],
    "user_cpf": [
      22222222222
    ],
    "transaction_type": [
      "TP_2"
    ]
  },
  {
    "user_balance": [
      2.25
    ],
    "transaction_code": [
      "d3c229140cf646829663cd40fa696aea"
    ],
    "transaction_data": [
      "2017-01-12T15:19:40"
    ],
    "user_name": [
      "Emerson Terceiro"
    ],
    "user_cpf": [
      33333333333
    ],
    "transaction_type": [
      "TP_1"
    ]
  },
  {
    "user_balance": [
      2.25
    ],
    "transaction_code": [
      "c7edd49432234e24b6673711cbf47c39"
    ],
    "transaction_data": [
      "2017-01-12T15:21:48"
    ],
    "user_name": [
      "Emerson Terceiro"
    ],
    "user_cpf": [
      33333333333
    ],
    "transaction_type": [
      "TP_1"
    ]
  }
]

```

## Comentários ##

* Para desenvolver os cálculos de cashback, foi utilizado o Padrão de projeto Chain of Responsability.
* Para realizar as requisições foi utilizado o Postman assim como indicado.
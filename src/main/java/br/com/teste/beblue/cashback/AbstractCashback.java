package br.com.teste.beblue.cashback;

import java.time.DayOfWeek;

import br.com.teste.beblue.models.Transaction;

public abstract class AbstractCashback implements ICashback {
	
	protected Double porcentagem;
	protected DayOfWeek dayTransaction;
	protected AbstractCashback nextDay;
	
	public Double calculaCashback(Transaction transaction) {
		return transaction.getTransactionValue() * porcentagem;
	}
	
	public Double getCashback(Transaction transaction){
		if(transaction.getTransactionDate().getDayOfWeek().equals(dayTransaction)){
			return calculaCashback(transaction);
		}
		return nextDay.getCashback(transaction);
	}
	
}

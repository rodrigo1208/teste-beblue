package br.com.teste.beblue.cashback;

import br.com.teste.beblue.models.Transaction;

public interface ICashback {
	Double calculaCashback(Transaction transaction);
}

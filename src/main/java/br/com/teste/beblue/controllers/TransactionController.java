package br.com.teste.beblue.controllers;

import java.io.IOException;
import java.net.URL;
import java.nio.charset.Charset;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;

import br.com.teste.beblue.cashback.CashbackManager;
import br.com.teste.beblue.daos.MerchantDAO;
import br.com.teste.beblue.daos.TransactionDAO;
import br.com.teste.beblue.daos.UserDAO;
import br.com.teste.beblue.models.Merchant;
import br.com.teste.beblue.models.Transaction;
import br.com.teste.beblue.models.TransactionReceiver;
import br.com.teste.beblue.models.User;

@Controller
public class TransactionController {
	
	@Autowired
	private TransactionDAO dao;
	@Autowired
	private UserDAO userDao;
	@Autowired
	private MerchantDAO merchantDao;
	
	private JSONArray responseBody = new JSONArray();
	private boolean equal;
	
	@RequestMapping(value="/transaction", method=RequestMethod.POST, consumes=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<HttpStatus> salva(@RequestBody Transaction transaction ){
		transaction.setTransactionDate(LocalDateTime.now());
		
		if(!existsTransactionType(transaction)){
			return new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);
		}
		
		if(transaction.getTransactionType().equals("TP_1")){
			dao.salva(transaction);
			return new ResponseEntity<>(HttpStatus.OK);
		}
		
		calculateEAddValueToUser(transaction);
		dao.salva(transaction);
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@RequestMapping(value="/usersTransactions", method=RequestMethod.GET)
	@ResponseBody
	private String getAllTransaction(){
		responseBody.toList().clear();
		dao.getAll().forEach(transaction ->{
			User user = userDao.getByCpf(transaction.getUserCpf());
			JSONObject newJson = new JSONObject();
			newJson.append("transaction_code", transaction.getTransactionCode())
						.append("user_cpf", user.getUserCpf())
						.append("user_name", user.getName())
						.append("user_balance", user.getBalance())
						.append("transaction_data", transaction.getTransactionDate())
						.append("transaction_type", transaction.getTransactionType());
			responseBody.put(newJson);
		});
		System.out.println(responseBody.toString(4));
		return responseBody.toString(4);
	}
	
	private List<TransactionReceiver> convertTransactionJson(){
		Gson gson = new Gson();
		List<TransactionReceiver> transactions = new ArrayList<>();
		JSONArray transactionsJson = null;
		try {
			transactionsJson = new JSONArray(IOUtils
			.toString(new URL("https://quarkbackend.com/getfile/vilibaldo-neto/json-javatest-transactiontypr")
					, Charset.forName("UTF-8")));
			transactionsJson.forEach(transaction -> {
				transactions.add(gson.fromJson(transaction.toString(), TransactionReceiver.class));
			});
		} catch (JSONException | IOException e) {
			e.printStackTrace();
		}
		return transactions;
	}
	
	private boolean existsTransactionType(Transaction transaction){
		equal = false;
		convertTransactionJson().forEach(trans -> {
			if(trans.getTransactionType().equals(transaction.getTransactionType())){
				equal = true;
			}
		});
		return equal;
	}
	
	private void calculateEAddValueToUser(Transaction transaction){
		User user = userDao.getByCpf(transaction.getUserCpf());
		Merchant merchant = merchantDao.getById(transaction.getMerchantId());
		//
		user.addBalance(CashbackManager.initCashback(merchant, transaction));
		userDao.salvaOuAtualiza(user);
	}
	
	
	
}

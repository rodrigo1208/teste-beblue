package br.com.teste.beblue.cashback;

import java.time.DayOfWeek;

import br.com.teste.beblue.models.Merchant;

public class Quarta extends AbstractCashback implements ICashback{
	
	public Quarta(Merchant merchant) {
		this.porcentagem = merchant.getQuarta();
		this.dayTransaction = DayOfWeek.WEDNESDAY;
		this.nextDay = new Quinta(merchant);
	}

}

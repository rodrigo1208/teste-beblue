package br.com.teste.beblue.daos;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.teste.beblue.models.Transaction;

@Repository
@Transactional
public class TransactionDAO {
	
	@PersistenceContext
	private EntityManager manager;
	
	public void salva(Transaction transaction){
		manager.persist(transaction);
	}
	
	public List<Transaction> getAll(){
		return manager.createQuery("from Transaction", Transaction.class).getResultList();
	}
	
}

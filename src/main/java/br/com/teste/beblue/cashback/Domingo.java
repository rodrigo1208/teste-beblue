package br.com.teste.beblue.cashback;

import java.time.DayOfWeek;

import br.com.teste.beblue.models.Merchant;

public class Domingo extends AbstractCashback{

	public Domingo(Merchant merchant) {
		this.porcentagem = merchant.getDomingo();
		this.dayTransaction = DayOfWeek.SUNDAY;
		this.nextDay = null;
	}
	
}

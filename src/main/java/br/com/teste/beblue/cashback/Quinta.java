package br.com.teste.beblue.cashback;

import java.time.DayOfWeek;

import br.com.teste.beblue.models.Merchant;

public class Quinta extends AbstractCashback {

	public Quinta(Merchant merchant) {
		this.porcentagem = merchant.getQuinta();
		this.dayTransaction = DayOfWeek.THURSDAY;
		this.nextDay = new Sexta(merchant);
	}
	
	
}

package br.com.teste.beblue.cashback;

import java.time.DayOfWeek;

import br.com.teste.beblue.models.Merchant;

public class Sabado extends AbstractCashback {

	public Sabado(Merchant merchant) {
		this.porcentagem = merchant.getSabado();
		this.dayTransaction = DayOfWeek.SATURDAY;
		this.nextDay = new Domingo(merchant);
	}
	
}

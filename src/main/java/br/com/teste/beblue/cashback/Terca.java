package br.com.teste.beblue.cashback;

import java.time.DayOfWeek;

import br.com.teste.beblue.models.Merchant;

public class Terca extends AbstractCashback {

	public Terca(Merchant merchant) {
		this.porcentagem = merchant.getTerca();
		this.dayTransaction = DayOfWeek.TUESDAY;
		this.nextDay = new Quarta(merchant);
	}

}

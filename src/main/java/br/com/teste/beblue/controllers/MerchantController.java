package br.com.teste.beblue.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import br.com.teste.beblue.daos.MerchantDAO;
import br.com.teste.beblue.models.Merchant;

@Controller
public class MerchantController {
	
	@Autowired
	private MerchantDAO dao;
	
	@RequestMapping(value="/merchant", method=RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public String salva(@RequestBody Merchant merchant){
		dao.salva(merchant);
		return "index";
	}
	
}

package br.com.teste.beblue.daos;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.teste.beblue.models.Merchant;

@Repository
@Transactional
public class MerchantDAO {
	
	@PersistenceContext
	private EntityManager manager;
	
	public void salva(Merchant merchant){
		manager.persist(merchant);
	}
	
	public Merchant getById(int id){
		return manager.createQuery("from Merchant where id=:id", Merchant.class)
				.setParameter("id", id)
				.getSingleResult();
	}
	
}

package br.com.teste.beblue.daos;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.teste.beblue.models.User;

@Repository
@Transactional
public class UserDAO {

	@PersistenceContext
	private EntityManager manager;
	
	public void salvaOuAtualiza(User user){
		if(!exists(user))
			manager.persist(user);
		if(user.getId() > 0)
			manager.merge(user);
	}
	
	private boolean exists(User user){
		return manager.createQuery("SELECT COUNT(u.userCpf) FROM User u WHERE u.userCpf=:cpf", Long.class)
							.setParameter("cpf", user.getUserCpf())
							.getSingleResult() > 0;
	}
	
	public User getByCpf(Long userCpf){
		return manager.createQuery("from User where userCpf=:userCpf", User.class)
					.setParameter("userCpf", userCpf)
					.getSingleResult();
	}
	
	public List<User> getAll(){
		return manager.createQuery("from User", User.class).getResultList();
	}
	
}

package br.com.teste.beblue.models;

public class TransactionReceiver {
	/*@JsonProperty("transaction_type")*/
	private String transaction_type;
/*	@JsonProperty("transaction_name")*/
	private String transaction_name;
	
	public String getTransactionType() {
		return transaction_type;
	}

	public void setTransactionType(String transactionType) {
		this.transaction_type = transactionType;
	}

	public String getTransactionName() {
		return transaction_name;
	}

	public void setTransactionName(String transactionName) {
		this.transaction_name = transactionName;
	}

	@Override
	public String toString() {
		return "Transaction [transaction_type=" + transaction_type + ", transaction_name=" + transaction_name + "]";
	}
	
	
}

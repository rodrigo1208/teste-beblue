package br.com.teste.beblue.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import br.com.teste.beblue.controllers.UserController;
import br.com.teste.beblue.daos.UserDAO;

@EnableWebMvc
@ComponentScan(basePackageClasses = { UserController.class, UserDAO.class })
public class AppConfiguration  {

	@Bean
	public InternalResourceViewResolver internalResourceViewResolver() {
		InternalResourceViewResolver resolver = new InternalResourceViewResolver();
		resolver.setPrefix("/WEB-INF/views/");
		resolver.setSuffix(".jsp");
		return resolver;
	}
}
